package com.example.davaleba8

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba8.databinding.ItemLayoutBinding
import com.example.davaleba8.databinding.ItemWithImageBinding

class UserAdapter(private val characters: MutableList<CharactersList>, private val Listener : InterfaceListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val USER_IMAGE_VIEW = 1
        private const val USER_VIEW  = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == USER_IMAGE_VIEW) {
            val itemView = ItemWithImageBinding.inflate(LayoutInflater.from(parent.context),parent, false)
            return UserImageViewHolder(itemView)
        }else{
            val itemView = ItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent, false)
            return UserViewHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserViewHolder -> holder.bind()
            is UserImageViewHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int = characters.size

    inner class UserImageViewHolder(val binding  : ItemWithImageBinding) : RecyclerView.ViewHolder(binding.root), View.OnLongClickListener, View.OnClickListener{
        private lateinit var modelImage : CharactersList
        fun bind(){
            modelImage = characters[adapterPosition]
            binding.imageView.setImageResource(modelImage.image?:R.mipmap.ic_launcher)
            binding.txtTitle.text = modelImage.name
            binding.txtAbout.text = modelImage.about
            binding.root.setOnLongClickListener(this)
            binding.root.setOnClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            Listener.userItemOnLongClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            Listener.itemOnClick(adapterPosition)
        }


    }
    inner class UserViewHolder(val binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root),View.OnLongClickListener, View.OnClickListener{
        private lateinit var modelImage : CharactersList
        fun bind(){
            modelImage = characters[adapterPosition]
            binding.txtTitle.text = modelImage.name
            binding.txtAbout.text = modelImage.about
            binding.root.setOnLongClickListener(this)
            binding.root.setOnClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            Listener.userItemOnLongClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            Listener.itemOnClick(adapterPosition)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val model = characters[position]
        return if(model.image== null){
            USER_VIEW
        }else{
            USER_IMAGE_VIEW
        }
    }
}