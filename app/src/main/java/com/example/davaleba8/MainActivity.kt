package com.example.davaleba8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.davaleba8.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private val characters = mutableListOf<CharactersList>()
    private lateinit var adapter: UserAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }
    private fun init(){
        dataSet()
        adapter = UserAdapter(characters, object : InterfaceListener{
            override fun userItemOnLongClick(position: Int) {
                characters.removeAt(position)
                Toast.makeText(this@MainActivity,"This character is removed", Toast.LENGTH_LONG).show()
                adapter.notifyItemRemoved(position)
            }
            override fun itemOnClick(position: Int) {
                Toast.makeText(this@MainActivity, " " + position + " is Clicked", Toast.LENGTH_LONG).show()
                adapter.notifyItemChanged(position)
            }
        })
        binding.recyclerView.layoutManager = GridLayoutManager(this,2)
        binding.recyclerView.adapter = adapter
    }
    private fun dataSet() {
        characters.add(CharactersList("Hello Kitty", "Cat with red bow",R.drawable.hellokitty))
        characters.add(CharactersList("Kuromi","Black cat"))
        characters.add(CharactersList("keropi", "frog",R.drawable.keropi))
        characters.add(CharactersList("my melody","cat"))
        characters.add(CharactersList("bad temaru", "black temaru",R.drawable.badtemaru))
        characters.add(CharactersList("froggie","small froggie"))
    }
}